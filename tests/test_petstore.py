import pytest
import requests

from ApiClient.api_requests import ApiRequests


class TestCheckPet:
    def test_check_pet(self, check_api_client: ApiRequests):
        post_req_pet = check_api_client.post_request_pet()
        get_req_pet_id = check_api_client.get_request_pet_by_id()
        get_req_pet_nonexistent_id = check_api_client.get_request_pet_by_nonexistent_id()
        get_req_pet_status = check_api_client.get_request_pet_by_status()
        put_req_pet = check_api_client.put_request_pet()
        put_req_nonexistent_pet = check_api_client.put_request_nonexistent_pet()
        del_req_pet = check_api_client.del_request_pet()
        assert post_req_pet.ok, f"Request failed with the status code {post_req_pet.status_code}"
        assert get_req_pet_id.ok, f"Request failed with the status code {get_req_pet_id.status_code}"
        assert not get_req_pet_nonexistent_id.ok, f"Request failed with the status code {get_req_pet_nonexistent_id.status_code}"
        assert get_req_pet_status.ok, f"Request failed with the status code {get_req_pet_status.status_code}"
        assert put_req_pet.ok, f"Request failed with the status code {put_req_pet.status_code}"
        assert not put_req_nonexistent_pet.ok, f"Request failed with the status code {put_req_nonexistent_pet.status_code}"
        assert post_req_pet.json()['category']['name'] != put_req_pet.json()['category']['name']
        assert del_req_pet.ok, f"Request failed with the status code {put_req_pet.status_code}"
        # assert get_req_pet_id.status_code == 404

class TestCheckStore:
    def test_check_store(self, check_api_client: ApiRequests):
        post_store_order = check_api_client.post_store()
        post_store_invalid_input = check_api_client.post_store_invalid_input()
        post_store_nonexistent_pet = check_api_client.post_store_nonexistent_pet()
        get_store_order_by_id = check_api_client.get_store_oder_by_id()
        get_store_order_by_invalid_id = check_api_client.get_store_oder_by_invalid_id()
        del_store_order = check_api_client.del_request_store_order()
        del_store_order_nonexistent_id = check_api_client.del_request_store_order_nonexistent_id()
        assert post_store_order.ok, f"Request failed with the status code {post_store_order.status_code}"
        assert not post_store_invalid_input.ok, f"Request failed with the status code {post_store_invalid_input.status_code}"
        assert not post_store_nonexistent_pet.ok, f"Request failed with the status code {post_store_nonexistent_pet.status_code}"
        assert get_store_order_by_id.ok, f"Request failed with the status code {get_store_order_by_id.status_code}"
        assert not get_store_order_by_invalid_id.ok, f"Request failed with the status code {get_store_order_by_invalid_id.status_code}"
        assert del_store_order.ok, f"Request failed with the status code {del_store_order.status_code}"
        assert not del_store_order_nonexistent_id.ok, f"Request failed with the status code {del_store_order_nonexistent_id.status_code}"

class TestCheckUsers:
    def test_check_users(self, check_api_client: ApiRequests):
        post_req_user = check_api_client.post_request_user()
        post_req_users_with_list = check_api_client.post_request_users_with_list()
        get_req_user = check_api_client.get_request_user()
        put_req_user = check_api_client.put_request_user()
        del_req_user = check_api_client.del_request_user()
        assert post_req_user.ok, f"Request failed with the status code {post_req_user.status_code}"
        assert post_req_users_with_list.ok, f"Request failed with the status code {post_req_users_with_list.status_code}"
        assert get_req_user.ok, f"Request failed with the status code {post_req_users_with_list.status_code}"
        assert put_req_user.ok, f"Request failed with the status code {put_req_user.status_code}"
        assert del_req_user.ok, f"Request failed with the status code {del_req_user.status_code}"

