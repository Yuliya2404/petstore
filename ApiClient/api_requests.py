import json
import os

import yaml

from ApiClient.abstract_api_client import ApiClient
from deserialization.users_des import users_deserialization


class ApiRequests(ApiClient):

    def post_request_pet(self):
        with open(os.path.join('.', 'files', 'pet.json')) as pet_file:
            body = json.load(pet_file)

        with open(os.path.join('.', 'files', 'headers.yml')) as yaml_file:
            headers = yaml.load(yaml_file, Loader=yaml.FullLoader)

        pet_req = self.post_pet('pet', body, headers)
        return pet_req

    def get_request_pet_by_id(self):
        with open(os.path.join(".", "files", "headers.yml")) as yaml_file:
            headers = yaml.load(yaml_file, Loader=yaml.FullLoader)

        get_req = self.get_pet_by_id(999, headers)
        return get_req

    def get_request_pet_by_nonexistent_id(self):
        with open(os.path.join(".", "files", "headers.yml")) as yaml_file:
            headers = yaml.load(yaml_file, Loader=yaml.FullLoader)

        get_req = self.get_pet_by_id(13579, headers)
        return get_req

    def get_request_pet_by_status(self):
        with open(os.path.join(".", "files", "headers.yml")) as yaml_file:
            headers = yaml.load(yaml_file, Loader=yaml.FullLoader)

        with open(os.path.join('.', 'files', 'status.json')) as status_file:
            params = json.load(status_file)

        get_req = self.get_pet_by_status('findByStatus', params, headers)
        return get_req

    def put_request_pet(self):
        with open(os.path.join('.', 'files', 'update.json')) as update_file:
            body = json.load(update_file)

        with open(os.path.join('.', 'files', 'headers.yml')) as yaml_file:
            headers = yaml.load(yaml_file, Loader=yaml.FullLoader)

        pet_req = self.put_pet('pet', body, headers)
        return pet_req

    def put_request_nonexistent_pet(self):
        with open(os.path.join('.', 'files', 'update_nonexistent_pet.json')) as update_file:
            body = json.load(update_file)

        with open(os.path.join('.', 'files', 'headers.yml')) as yaml_file:
            headers = yaml.load(yaml_file, Loader=yaml.FullLoader)

        pet_req = self.put_pet('pet', body, headers)
        return pet_req

    def del_request_pet(self):
        pet_req = self.del_pet(999)
        return pet_req

    def post_store(self):
        with open(os.path.join('.', 'files', 'store.json')) as store_file:
            body = json.load(store_file)

        with open(os.path.join(".", "files", "headers.yml")) as yaml_file:
            headers = yaml.load(yaml_file, Loader=yaml.FullLoader)

        post_req = self.post_store_order('order', body, headers)
        return post_req

    def post_store_invalid_input(self):
        with open(os.path.join('.', 'files', 'store_invalid_input.json')) as store_file:
            body = json.load(store_file)

        with open(os.path.join(".", "files", "headers.yml")) as yaml_file:
            headers = yaml.load(yaml_file, Loader=yaml.FullLoader)

        post_req = self.post_store_order('order', body, headers)
        return post_req

    def post_store_nonexistent_pet(self):
        with open(os.path.join('.', 'files', 'store_nonexistent_pet.json')) as store_file:
            body = json.load(store_file)

        with open(os.path.join(".", "files", "headers.yml")) as yaml_file:
            headers = yaml.load(yaml_file, Loader=yaml.FullLoader)

        post_req = self.post_store_order('order', body, headers)
        return post_req

    def get_store_oder_by_id(self):
        get_req = self.get_store_order_by_id(10)
        return get_req

    def get_store_oder_by_invalid_id(self):
        get_req = self.get_store_order_by_id(100)
        return get_req

    def del_request_store_order(self):
        del_req = self.del_store_order(10)
        return del_req

    def del_request_store_order_nonexistent_id(self):
        del_req = self.del_store_order(1001)
        return del_req

    def post_request_user(self):
        with open(os.path.join('.', 'files', 'user.json')) as user_file:
            body = json.load(user_file)

        with open(os.path.join(".", "files", "headers.yml")) as yaml_file:
            headers = yaml.load(yaml_file, Loader=yaml.FullLoader)

        post_req = self.post_user('user', body, headers)
        return post_req

    def post_request_users_with_list(self):
        body = users_deserialization(os.path.join('.', 'files', 'users.csv'))
        json.dumps(body, indent=4)

        with open(os.path.join(".", "files", "headers.yml")) as yaml_file:
            headers = yaml.load(yaml_file, Loader=yaml.FullLoader)

        post_req = self.post_users_with_list('createWithList', body, headers)
        return post_req


    def get_request_user(self):
        with open(os.path.join(".", "files", "headers.yml")) as yaml_file:
            headers = yaml.load(yaml_file, Loader=yaml.FullLoader)

        get_req = self.get_user('Mariia24', headers)
        return get_req

    def put_request_user(self):
        with open(os.path.join('.', 'files', 'update_user.json')) as user_file:
            body = json.load(user_file)

        with open(os.path.join(".", "files", "headers.yml")) as yaml_file:
            headers = yaml.load(yaml_file, Loader=yaml.FullLoader)

        get_req = self.put_user('Mariia24', body, headers)
        return get_req

    def del_request_user(self):
        del_req = self.del_user('Mariia24')
        return del_req






