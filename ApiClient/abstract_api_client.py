import json
import os

import requests as requests


class ApiClient:
    def __init__(self):
        self.__client = requests.Session()

    def post_pet(self, endpoint, body, headers):
        url = f'https://petstore3.swagger.io/api/v3/{endpoint}'
        res = self.__client.post(url, json=body, headers=headers)
        return res

    def get_pet_by_id(self, endpoint, headers):
        url = f'https://petstore3.swagger.io/api/v3/pet/{endpoint}'
        res = self.__client.get(url, headers=headers)
        return res

    def get_pet_by_status(self, endpoint, params, headers):
        url = f"https://petstore3.swagger.io/api/v3/pet/{endpoint}"
        res = self.__client.get(url, params=params, headers=headers)
        return res

    def put_pet(self, endpoint, body, headers):
        url = f'https://petstore3.swagger.io/api/v3/{endpoint}'
        res = self.__client.put(url, json=body, headers=headers)
        return res

    def del_pet(self, endpoint):
        url = f'https://petstore3.swagger.io/api/v3/pet/{endpoint}'
        res = self.__client.delete(url)
        return res

    def post_store_order(self, endpoint, body, headers):
        url = f'https://petstore3.swagger.io/api/v3/store/{endpoint}'
        res = self.__client.post(url, json=body, headers=headers)
        return res

    def get_store_order_by_id(self, endpoint):
        url = f'https://petstore3.swagger.io/api/v3/store/order/{endpoint}'
        res = self.__client.get(url)
        return res

    def del_store_order(self, endpoint):
        url = f'https://petstore3.swagger.io/api/v3/store/order/{endpoint}'
        res = self.__client.delete(url)
        return res

    def post_user(self, endpoint, body, headers):
        url = f'https://petstore3.swagger.io/api/v3/{endpoint}'
        res = self.__client.post(url, json=body, headers=headers)
        return res

    def post_users_with_list(self, endpoint, body, headers):
        url = f'https://petstore3.swagger.io/api/v3/user/{endpoint}'
        res = self.__client.post(url, json=body, headers=headers)
        return res

    def get_user(self, endpoint, headers):
        url = f'https://petstore3.swagger.io/api/v3/user/{endpoint}'
        res = self.__client.get(url, headers=headers)
        return res

    def put_user(self, endpoint, body, headers):
        url = f'https://petstore3.swagger.io/api/v3/user/{endpoint}'
        res = self.__client.put(url, json=body, headers=headers)
        return res

    def del_user(self, endpoint):
        url = f'https://petstore3.swagger.io/api/v3/store/user/{endpoint}'
        res = self.__client.delete(url)
        return res

    def __del__(self):
        self.__client.close()




